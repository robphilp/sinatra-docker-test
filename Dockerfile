# Rob's Docker sinatra test file

# FROM ubuntu:14.04
# FROM dock0/arch:latest
# MAINTAINER Rob Philp <robert.h.philp@gmail.com>

# Use phusion/baseimage as base image. To make your builds reproducible, make
# sure you lock down to a specific version, not to `latest`!
# See https://github.com/phusion/baseimage-docker/blob/master/Changelog.md for
# a list of version numbers.
FROM phusion/baseimage:0.9.9
MAINTAINER Rob Philp <robert.h.philp@gmail.com>

# Use baseimage-docker's init system.
CMD ["/sbin/my_init"]

# ...put your own build instructions here...
RUN apt-get update && apt-get install -y curl
RUN gpg --keyserver hkp://keys.gnupg.net --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3

RUN \curl -sSL https://get.rvm.io | bash -s stable
RUN bash -l -c "rvm install 2.2"
RUN bash -l -c "gem install bundler"
RUN bash -l -c "gem install sinatra"
RUN bash -l -c "gem install sinatra-contrib"
RUN bash -l -c "cd /home; mkdir sinatra;"

# Clean up APT when done.
RUN apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

EXPOSE 4567