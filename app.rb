require 'sinatra'
require 'sinatra/reloader' if development?

set :bind, '0.0.0.0'

get '/:greeting?/:name?' do |name|
  name ||= "Robert"
  "Hello #{name}!!"
end